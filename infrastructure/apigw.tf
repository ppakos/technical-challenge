#+--------------------------------------------------------------------+
#|                          Resources                                 |
#+--------------------------------------------------------------------+

resource "aws_api_gateway_rest_api" "simple_api" {
  name        = "SimpleApi"
  description = "API Gateway resource for the simple NodeJS API"
}


#+--------------------------------------------------------------------+
#|                         Deployment                                 |
#+--------------------------------------------------------------------+

resource "aws_api_gateway_deployment" "simple_api" {
   depends_on = [
     aws_api_gateway_integration.lambda,
     aws_api_gateway_integration.lambda_root,
   ]

   rest_api_id = aws_api_gateway_rest_api.simple_api.id
   stage_name  = var.env
}

#+--------------------------------------------------------------------+
#|                            Outputs                                 |
#+--------------------------------------------------------------------+
output "base_url" {
  value = aws_api_gateway_deployment.simple_api.invoke_url
}
