#+--------------------------------------------------------------------+
#|                             TF Config                              |
#+--------------------------------------------------------------------+

terraform {
  backend "s3" {}
}

#+--------------------------------------------------------------------+
#|                             Variables                              |
#+--------------------------------------------------------------------+

variable "aws_access_key" {
    type        = string
    description = "Access key for AWS user"
}

variable "aws_secret_key" {
    type        = string
    description = "Secret key for AWS user"
}

variable "region" {
    default     = "eu-west-2"
    type        = string
    description = "AWS Region to use"
}

variable "env" {
    type        = string
    description = "Name of the current environment"    
}

variable "project_version" {
    default = "0.1"
    type = string
    description = "project version"
}

variable "bucket_name" {
    type = string
    description = "name for the pipeline S3 bucket"
}

#+--------------------------------------------------------------------+
#|                             Providers                              |
#+--------------------------------------------------------------------+

provider "aws" {
    access_key = var.aws_access_key
    secret_key = var.aws_secret_key
    region     = var.region
}