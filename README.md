# Technical challenge

This repository has the purpose to showcase the result of the technical challenge.
It is composed by a simple API in NodeJS deployed in AWS Lambda and exposed through API Gateway and a unit test.
There is also an infrastructure component automated using Terraform and Gitlab CI.

## NodeJS API
The API is a simple function that returns "Hello" in a randomly selected language or a waving hand emoji if the language selected is not available.
The unit tests are managed using Jest and after running them, a report is produced. On the example provided there is one test case with two asserts, one for an available language and one for a language not present in the list. The sample API and test were taken from this [NodeJS Tutorial](https://www.serverless.com/blog/unit-testing-nodejs-serverless-jest). Slight changes from the tutorial were made for showcasing a passing test and a failing test, as well as the way to run the tests. The example did not include a report that can be integrated with GitlabCI so **the report generation was added** to the pipeline.

![restapi.png](restapi.png "REST API")

The failing test example can be found in the branch [feature/failing-test](https://gitlab.com/dfonseca97/technical-challenge/-/tree/feature/failing-test). There is also an example merge request where the tests' summary can be seen.

The live running API can be found at: https://rqm4zoxzid.execute-api.eu-west-2.amazonaws.com/test

## Infrastructure
The created function is deployed using AWS Lambda and API Gateway. A Terraform module for these components was created and is available in the "infrastructure" folder. It is made up of three Terraform files:

1. **main.tf** contains all the variables required and the AWS provider configuration
2. The **lambda.tf** file contains the Lambda function, a role to execute the function, and the integration with API Gateway as well as the permission to execute the function.
3. **apigw.tf** contains the deployment of the Lambda function in API Gateway and it also outputs the public url to access the function.

There is also a "backend" folder containing the remote backend information for Terraform to manage the state file. This is required for the automation process to make sure the current state of the infrastructure is taken into account when running the pipeline. It also provides the benefit of locking the state so that no conflicts occur when updating the infrastructure. This module only contains a main.tf file that only needs to be used once to create the needed elements for the backend:

- A bucket to store the state file
- A DynamoDB table to control the state locking

![infrastructure.png](infrastructure.png "Infrastructure")

This is part of the project setup so **it should be run once before the project infrastructure module**. Please note that it requires the AWS account acces key and secret key to set up the bucket and the DynamoDB table in your own account. The default region setup for the infrastructure is eu-west-2 (London).

```bash
cd infrastructure/backend
```
```bash
terraform plan
```
```bash
terraform apply
```
## Automation

### Environment Variables
The CI and CD pipeline was created using GitlabCI. A .gitlab-ci.yml file is available in the repository root and it contains all the instructions for the pipeline. Before explaining the steps be aware that some repository secret variables are needed for the pipeline to work:
- **TF_VAR_aws_access_key:** Access key for the AWS account
- **TF_VAR_aws_secret_key:** Secret key for the AWS Account
- **TF_VAR_bucket_name:** The name of the bucket for the pipeline. It should be **the same bucket used when creating the Terraform backend**.
- **TF_VAR_bucket_key:** The location for the Terraform remote backend inside the S3 Bucket
- **TF_VAR_env:** The environment for this deployment. Used mostly in the infrastructure tags in AWS.
- **TF_VAR_project_version:** The project version. This also affects where the lambda function will be stored in S3 to keep track of the produced artifacts. It can be changed when updating to a new version to also create a new folder with the artifacts.
- **TF_VAR_region:** The region inside AWS. Defaults to eu-west-2 (London).
- **TF_VAR_table_name:** The table name for the Terraform state locking. It should be the same as the backend configuration table name which in this case is always "remote-state-lock".

These variables are required to be setup before running the project and can be changed when it is needed. At the moment they are stored in the secret variables configuration of this repository for this example.

### Steps
The pipeline has the following steps:
1. **test:** The code is tested using Jest and a report is generated for GitlabCI to show it in merge requests and the pipeline jobs results. This step uses a Node image for the purpose of having easy access to yarn or npm.
2. **push_code:** Since the API is a serverless function, AWS Lambda needs to get the code from somewhere. In this case, the bucket previously defined. Once the code has been tested, it is zipped and uploaded to the S3 bucket for Lambda to obtain it as it is defined in the lambda.tf file. This step uses a python image to install the AWS CLI through pip. 
3. **deploy:** The last step in the pipeline. It connects to the remote backend specified before, validates the Terraform files and updates the code inside the Lambda.

![stepspng.png](stepspng.png "Pipelinesteps")

After these steps are completed, the function will be updated. The code update and the deployment will only run when pushing a commit to the master branch. The test step runs in all branches.
